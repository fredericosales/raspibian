#!/bin/bash
#
# Frederico Sales
# <frederico.sales@engenharia.ufjf.br>
# 2019
#
# Raspibian stretch basic packages
#

# install some basic packages
clear;
echo "Updating...";
echo;
sudo apt update;
sudo apt upgrade -y;

clear;
echo "Install packages...";
echo;
sudo apt install -y vim \
	git \
	p7zip-full \
	ranger \
	tmux \
	mtr \
	htop \
	nmap \
	vim \
	iptraf \
	wavemon \
	build-essential \
	nfs-kernel-server \
	nfs-common \
	cmake \
	bc \
	autogen \
	pkg-config \
	llvm \
	llvm-dev \
	libeigen3-dev \
	libarmadillo-dev \
	python3-dev \
	python3-pip \
	python3-numpy \
	python3-matplotlib \
	virtualenv \
	virtualenvwrapper \
	cups \
	funcoeszz;

# checking apt 
clear;
echo "Checking...";
echo;
sudo apt -f install;
sudo apt -y autoremove;
sudo apt -y  autoclean;

# cups
clear;
sudo cupsctl -remote-any
sudo systemctl restart cups.service


# Updating .bashrc
clear;
echo "Updating .bashrc...";
echo;
echo "# me myself and I" >> $HOME/.bashrc
echo "export PS1=\"\[\033[38;5;11m\]\u\[$(tput sgr0)\]\[\033[38;5;15m\]@\h:\[$(tput sgr0)\]\[\033[38;5;6m\][\w]:\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]\"" >> $HOME/.bashrc
echo " " >> $HOME/.bashrc
echo "# virtualenv" >> $HOME/.bashrc
echo "export WORKON_HOME=$HOME/virtualenvs" >> $HOME/.bashrc
echo "source /usr/share/virtualenvwrapper/virtualenvwrapper.sh" >> $HOME/.bashrc
echo " " >> $HOME/.bashrc
echo "# Instalacao das Funcoes ZZ (www.funcoeszz.net)" >> $HOME/.bashrc
echo "export ZZOFF=\" \"  # desligue funcoes indesejadas" >> $HOME/.bashrc
echo "export ZZPATH=\"/usr/bin/funcoeszz\"  # script " >> $HOME/.bashrc
echo "source \"$ZZPATH\"" >> $HOME/.bashrc
echo " " >> $HOME/.bashrc

