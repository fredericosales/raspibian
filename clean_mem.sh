#!/bin/bash
#
# Frederico Sales
# <frederico.sales@engenharia.ufjf.br>
# 2019
#
clear;
echo "Cleaning mem...";
echo;
sudo swapoff -a;
sudo swapon -a;
echo "echo 3 > /proc/sys/vm/drop_caches" | sudo  sh;
