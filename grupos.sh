#!/bin/bash
#

# VAR
USER='frederico'

clear;
echo "Adicionando aos grupos..."
sudo usermod -aG pi $USER;
sudo usermod -aG adm $USER;
sudo usermod -aG dialout $USER;
sudo usermod -aG cdrom $USER;
sudo usermod -aG sudo $USER;
sudo usermod -aG audio $USER;
sudo usermod -aG video $USER;
sudo usermod -aG plugdev $USER;
sudo usermod -aG games $USER;
sudo usermod -aG users $USER;
sudo usermod -aG input $USER;
sudo usermod -aG netdev $USER;
sudo usermod -aG gpio $USER;
sudo usermod -aG i2c $USER;
sudo usermod -aG spi $USER;
echo
echo "O usuario $USER foi adiocionado com sucesso aos grupos."
echo "Done."

