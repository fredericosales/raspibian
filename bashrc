# me myself and I
export PS1="\[\033[38;5;11m\]\u\[(B[m\]\[\033[38;5;15m\]@\h:\[(B[m\]\[\033[38;5;6m\][\w]:\[(B[m\]\[\033[38;5;15m\] \[(B[m\]"
 
# virtualenv
export WORKON_HOME=/home/frederico/virtualenvs
source /usr/share/virtualenvwrapper/virtualenvwrapper.sh
 
# Instalacao das Funcoes ZZ (www.funcoeszz.net)
export ZZOFF=" "  # desligue funcoes indesejadas
export ZZPATH="/usr/bin/funcoeszz"  # script 
source "/usr/bin/funcoeszz"
 
